#!/bin/bash

apt-get update
apt-get --yes --force-yes install build-essential
apt-get --yes --force-yes install default-jre
TEMP=~

# Install R, R-Server, and R-Studio
mkdir ~/sfw/r
cd ~/sfw/r/
apt-get --yes --force-yes install r-base
apt-get --yes --force-yes install gdebi-core
apt-get --yes --force-yes install libapparmor1
wget http://download2.rstudio.org/rstudio-server-0.98.1103-amd64.deb
gdebi rstudio-server-0.98.1103-amd64.deb 
wget http://download1.rstudio.org/rstudio-0.98.1103-amd64.deb
gdebi rstudio-0.98.1103-amd64.deb 

# Install STAR
mkdir ~/sfw/star
cd ~/sfw/star/
git init
git remote add origin https://github.com/alexdobin/STAR
git pull origin master
cp bin/Linux_x86_64_static/STAR /usr/local/bin

# Install Subread
mkdir ~/sfw/subread
cd ~/sfw/subread/
wget http://sourceforge.net/projects/subread/files/subread-1.4.6-p3/subread-1.4.6-p3-Linux-x86_64.tar.gz/download
mv download subread-1.4.6-p3-Linux-x86_64.tar.gz
gunzip subread-1.4.6-p3-Linux-x86_64.tar.gz
tar xvf subread-1.4.6-p3-Linux-x86_64.tar
cd subread-1.4.6-p3-Linux-x86_64/
cp ./* /usr/local/bin
cd utilities/
cp ./* /usr/local/bin

# Install HTSeq
mkdir ~/sfw/htseq
cd ~/sfw/htseq/
apt-get --yes --force-yes install python2.7-dev
apt-get --yes --force-yes install python-numpy
apt-get --yes --force-yes install python-matplotlib
wget https://pypi.python.org/packages/source/H/HTSeq/HTSeq-0.6.1.tar.gz
gunzip HTSeq-0.6.1.tar.gz 
tar xvf HTSeq-0.6.1.tar 
cd HTSeq-0.6.1/
python setup.py build
python setup.py --yes --force-yes install

# Install BBMap
mkdir ~/sfw/bbmap
cd ~/sfw/bbmap/
wget https://sourceforge.net/projects/bbmap/files/latest/download
mv download bbmap.tar.gz
gunzip bbmap.tar.gz 
tar xvf bbmap.tar 
cd bbmap/
cp -R ./* /usr/local/bin

# Install FastQC
mkdir ~/sfw/fastqc
cd ~/sfw/fastqc/
wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.3.zip
unzip fastqc_v0.11.3.zip
ln -s $TEMP/fastqc/FastQC/fastqc /usr/local/bin/fastqc

# Install SAMtools
mkdir ~/sfw/samtools
cd ~/sfw/samtools/
wget https://github.com/samtools/samtools/releases/download/1.2/samtools-1.2.tar.bz2
bunzip2 samtools-1.2.tar.bz2
tar xvf samtools-1.2.tar
cd samtools-1.2/
make
make prefix=/usr/local install

# Install BEDTools
apt-get --yes --force-yes install bedtools

# Install Picard
apt-get --yes --force-yes install binfmt-support
mkdir ~/sfw/picard
cd ~/sfw/picard/
wget https://github.com/broadinstitute/picard/releases/download/1.131/picard-tools-1.131.zip
unzip picard-tools-1.131.zip
cd picard-tools-1.131/
cp *jar /usr/local/bin
chmod 755 /usr/local/bin/*jar

