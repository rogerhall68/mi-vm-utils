#!/bin/bash

apt-get update
apt-get --yes --force-yes install build-essential
apt-get --yes --force-yes install default-jre
TEMP=~

# Install R, R-Server, and R-Studio
mkdir ~/sfw/r
cd ~/sfw/r/
apt-get --yes --force-yes install r-base
apt-get --yes --force-yes install gdebi-core
apt-get --yes --force-yes install libapparmor1
wget http://download2.rstudio.org/rstudio-server-0.99.902-amd64.deb
gdebi rstudio-server-0.99.902-amd64.deb 
wget http://download1.rstudio.org/rstudio-0.99.902-amd64.deb
gdebi rstudio-0.99.902-amd64.deb
